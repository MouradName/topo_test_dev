<!-- Page principale qui permet d'afficher les différentes requêtes -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Principale</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <ul>
    <li><a href="index.php">Accueil</a></li>
    <li><a href="note_moyenne_territoire.php">La note moyenne par territoire</a></li>
    <li><a href="affichage_commentaire_commerce.php">L'affichage du commentaire pour le commerce</a></li>
    <li><a href="formulaire_ajout_territoire.php">Formulaire d'ajout de territoire</a></li>
    </ul>

</body>
</html>

