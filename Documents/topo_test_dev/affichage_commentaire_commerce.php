<?php 

include('bdd.php'); 


// Test : La même page par commerce, avec affichage des commentaires
if (isset($_POST['btnCommerce'])) {
    $name = htmlspecialchars(trim($_POST['name']));

$query = $bdd->prepare('INSERT INTO commerce(name) VALUES(:name)');
$query->execute(array(
      'name' => $name
       ));
}

$moyenneCommerce = array();
$queryMoyCom = $bdd->prepare('SELECT AVG(dat.note) as averageNote, com.name as nameCommerce, dat.commerce, dat.fcom as nameCommentaire
FROM data as dat
inner join commerce as com
on dat.commerce = com.commerce
GROUP BY dat.commerce');
$queryMoyCom->execute();
while($row = $queryMoyCom->fetch(PDO::FETCH_ASSOC))
{
    array_push($moyenneCommerce,$row);
} 

?>


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Affichage du commentaire du commerce</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>

<body>
    <?php include('index.php') ?>

    <table>
        <tr>
            <th>Nom du commerce</th>
            <th>Note</th>
            <th>Commentaires</th>
        </tr>
        <?php foreach($moyenneCommerce as $item)
            echo '<tr><td>'.$item['nameCommerce'].'</td><td>'.$item['averageNote'].'</td><td>'.$item['nameCommentaire'].'</td></tr>';
        ?>
    </table>

</body>

</html>







