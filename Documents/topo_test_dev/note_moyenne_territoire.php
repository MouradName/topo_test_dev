<?php 
// inclure le fichier php qui permet de connecter à mySQL
include('bdd.php'); 

// Test :  Une page php permettant de lister la note moyenne par territoire (avec le nom et pas l’index)
// script php permettant de lister la note moyenne par territoire
$moyenneTerritoire = array();
$queryMoyTer = $bdd->prepare('SELECT AVG(dat.note) as averageNote, terri.name as nameTerritoire, dat.territoire
    FROM data as dat
    inner join territoire as terri
    on dat.territoire = terri.territoire
    GROUP BY dat.territoire');
$queryMoyTer->execute();
while($row = $queryMoyTer->fetch(PDO::FETCH_ASSOC))
{
    array_push($moyenneTerritoire,$row);
} 

?>

<!DOCTYPE html>
<html>


<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Lister la note moyenne par territoire</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>


<body>
<?php 
// inclure le fichier php qui permet de choisir l'affichage 
include('index.php') 
?>
    <table>
        <tr>
            <th>Nom du territoire</th>
            <th>Note moyenne du territoire</th>
        </tr>
        <?php foreach($moyenneTerritoire as $item)
                echo '<tr><td>'.$item['nameTerritoire'].'</td><td>'.$item['averageNote'].'</td></tr>';
            ?>
    </table>
</body>

</html>
