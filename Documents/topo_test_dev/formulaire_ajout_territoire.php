<?php 
// inclure le fichier php qui permet de connecter à mySQL
include('bdd.php'); 

// Test : un formulaire permettant d’ajouter un territoire
if (isset($_POST['btnTerritoire'])) {
    $name = htmlspecialchars(trim($_POST['name']));

$query = $bdd->prepare('INSERT INTO territoire(name) VALUES(:name)');
$query->execute(array(
      'name' => $name
       ));

}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ajout d'un territoire</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <?php include('index.php') ?>

    <form action="#" method="POST">
        <table>
            <h1>Ajout d'un territoire</h1>
                <tr>
                    <th><label for="name">Nom du territoire : </label></th>
                    <td><input name="name"></td>
                </tr>
                <tr>
                    <td>
                    <button type="submit" name="btnTerritoire">Ajouter un
                                territoire</button>
                     </td>
                </tr>
        </table>
    </form>

</body>

</html>
